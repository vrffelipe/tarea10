#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 17:59:35 2021

@author: francisco
"""

from os import listdir
from os.path import isfile, join
import tweepy
import json
import csv
from datetime import date
from datetime import datetime, timedelta
from pandas.core.common import flatten
#####
with open('palabras2.csv', newline='') as f:
    reader = csv.reader(f)
    terminos = list(reader)
terminos=list(flatten(terminos))
terminos.pop(0)
otros=["mujeres", "feminicidios"]
terminos=terminos+ otros
#terminos=["#yasienteseseñora", "urgida", "femilocas", "FAKEMINISTAS", "Bieja"]
####
consumer_key="xxxxxx"
consumer_secret="xxxxx"

access_token="xxxxx"
access_token_secret="xxxxxxxxx"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True)
#api = tweepy.API(auth, wait_on_rate_limit=True)
places = api.geo_search(query="Mexico", granularity="country")
place_id = places[0].id

#terminos="mujeres"
busquedas=50000

genb="/home/francisco/Documentos/Personal/Indice/20200601/"
fin = date.today()
inicio=fin-timedelta(7)
inicio = inicio.strftime("%Y-%m-%d")
fin = fin.strftime("%Y-%m-%d")

#terminos=terminos[53:]
#tweets = tweepy.Cursor(api.search , q=(terminos) and ("place:%s" % place_id)).items(busquedas)

#terminos = ['Mujeres']
def stream_tweets(search_term, busquedas, place_id):
    data = [] # empty list to which tweet_details obj will be added
    counter = 0 # counter to keep track of each iteration
    for tweet in tweepy.Cursor(api.search,  q='{} place:{}'.format(search_term, place_id) , since=inicio, until = fin, tweet_mode='extended').items(busquedas):
        tweet_details = {}
        tweet_details['name'] = tweet.user.screen_name
        tweet_details['tweet'] = tweet.full_text
        tweet_details['retweets'] = tweet.retweet_count
        tweet_details['location'] = tweet.user.location
        tweet_details['created'] = tweet.created_at.strftime("%d-%b-%Y")
        tweet_details['followers'] = tweet.user.followers_count
        tweet_details['is_user_verified'] = tweet.user.verified
        data.append(tweet_details)
        
        counter += 1
        if counter == 1000:
            break
        else:
            pass
    with open(genb+'{}.json'.format(search_term), 'w') as f:
        json.dump(data, f, ensure_ascii=False)
    print('¡finalizado!')
for search_term in terminos:
        stream_tweets(search_term, busquedas,place_id)
    