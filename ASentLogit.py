# -*- coding: utf-8 -*-
"""
Created on Sat Jun 12 13:39:29 2021

@author: Z820-1
"""

import csv
import jieba
import numpy as np
import json
import pandas as pd
from pandas import DataFrame
import matplotlib.pyplot as plt
from collections import OrderedDict
import re
from itertools import chain
from timeit import Timer
import timeit
import string
import random
import matplotlib.colors as mcolors
from itertools import product
import itertools
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer  
from sklearn.metrics import roc_auc_score
import unicodedata
from nltk import word_tokenize 
from sklearn.metrics import roc_curve, auc
from sklearn.svm import LinearSVC
from yellowbrick.text import FreqDistVisualizer
from numpy import mean
from numpy import std
from sklearn.datasets import make_classification
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB  #Naive Bayes
from sklearn.model_selection import GridSearchCV, StratifiedKFold, train_test_split
from sklearn.metrics import make_scorer, log_loss, confusion_matrix
from sklearn.linear_model import LogisticRegression


path='Prueba_V002.csv'
df = pd.read_csv(path)
path1='Prediccion_V001.csv'
prede = pd.read_csv(path1)
#Se define un porcentaje 
porcentaje = 0.80 # Porcentaje de train.
#Selección aleatoria de la muestra de entrenamiento y de prueba
df['entrenamiento'] = np.random.uniform(0, 1, len(df)) <= porcentaje
entrenamiento=df[df['entrenamiento']==True]
entrenamiento.reset_index(drop=True, inplace=True)
prueba=df[df['entrenamiento']==False]
prueba.reset_index(drop=True, inplace=True)
#Se borra la columna creada
del entrenamiento['entrenamiento']
del prueba['entrenamiento']

def limpieza(df1):
        #Se convierten en minúsculas
    df1['name'] = [oracion.lower() for oracion in df1['name']]
    df1['name']=df1['name'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    #Se quitan direcciones
    df1['name']=df1['name'].str.replace(r'https*\S+', "", regex=True)
    #Se quitan usuarios
    df1['name']=df1['name'].str.replace(r'@\S+', "", regex=True)
    #Se la palabra gracias
    df1['name']=df1['name'].str.replace(r'gracais', "", regex=True)
    #Se quitan hashtags
    df1['name']=df1['name'].str.replace(r'#\S+', "", regex=True)
    df1['name']=df1['name'].str.replace(r'\'\w+', "", regex=True)
    #Se quitan signos de puntuación
    df1['name']=df1['name'].str.replace(r'[^\w\s]', "", regex=True)
    #Se quitan dobles espacios
    df1['name']=df1['name'].str.replace(r'\s{2,}', "", regex=True)
    #Se remueven signos especiales
    df1['name']=df1['name'].str.replace(r"\d+", " ", regex=True)
    #============================================
    #Se hace una lista con cada palabra del texto
    #============================================ 
    lista = df1["name"].values.tolist()
    lista1 = df1["Clasificación"].values.tolist()
    #============================================================
    #Las letras que se repiten consecutivamente se quitan por una 
    #============================================================
    repeat_pattern = re.compile(r'(\w)\1*')
    match_substitution = r'\1'
    lista=[repeat_pattern.sub(r'\1', i) for i in lista]
    return lista, lista1

#Entropía cruzada
#Donde p(x) es la probabilidad deseada, y q(x) la probabilidad real. 
#La suma está sobre las tres clases A y B . En este caso 0 y 1. 

def cross_entropy( targets,predictions, epsilon=1e-12):
    """
    Computes cross entropy between targets (encoded as one-hot vectors)
    and predictions. 
    Input: predictions (N, k) ndarray
           targets (N, k) ndarray        
    Returns: scalar
    """
    predictions bitbucket as invitate= np.clip(predictions, epsilon, 1. - epsilon)
    N = predictions.shape[0]
    ce = -np.sum(targets*np.log(predictions+1e-9))/N
    return ce

#Se tienen 2 tablas,una con el texto y otra con la clasificación para la parte de la tabla para entrenar
entrena_x,entrena_y=limpieza(entrenamiento)

#Se tienen 2 tablas,una con el texto y otra con la clasificación para la parte de la tabla para probar
prueba_x,prueba_y=limpieza(prueba)

#Se tienen 2 tablas,una con el texto y otra con la clasificación para la parte de la tabla para probar
prede_x,prede_y=limpieza(prede)


#########################
#Entrenamiento del modelo
#from sklearn.svm import SVC
#param_grid = {'C': [0.1, 1, 10, 100, 1000], 
 #             'gamma': [1, 0.1, 0.01, 0.001, 0.0001],
  #            'kernel': ['rbf']} 

#estimator = GridSearchCV(SVC(), verbose=1, param_grid=param_grid,
#                        scoring='roc_auc', n_jobs=18)

estimator=LogisticRegression()

######################
#Aplicación del modelo
vectorizer = CountVectorizer(stop_words=stopwords.words('spanish'))  
x_vec = vectorizer.fit_transform(entrena_x)
features   = vectorizer.get_feature_names()

tfidfvec = TfidfVectorizer(stop_words=stopwords.words('spanish'))  
x1_vec = tfidfvec.fit_transform(entrena_x)
features1   = vectorizer.get_feature_names()

estimator.fit(x_vec, entrena_y)

x1_gold_vec = tfidfvec.transform(prueba_x)
mnb_predict1 = estimator.predict(x1_gold_vec)
np.unique(mnb_predict1, return_counts=True)

x2_gold_vec = vectorizer.transform(prueba_x)
mnb_predict2 = estimator.predict(x2_gold_vec)
np.unique(mnb_predict2, return_counts=True)


from sklearn.metrics import accuracy_score, f1_score
###################
#Medidas del modelo
#vectorizer
print("Acc MB =", accuracy_score(prueba_y,mnb_predict1))
print("F1 MB =", f1_score(prueba_y, mnb_predict1, average='macro'))
print("micro F1 MB =", f1_score(prueba_y, mnb_predict1, average='micro'))
print("Entropía cruzada", cross_entropy(prueba_y, mnb_predict1))

###################
#Medidas del modelo
#tfidfvec
print("Acc MB =", accuracy_score(prueba_y,mnb_predict2))
print("F1 MB =", f1_score(prueba_y, mnb_predict2, average='macro'))
print("micro F1 MB =", f1_score(prueba_y, mnb_predict2, average='micro'))
print("Entropía cruzada", cross_entropy(prueba_y,mnb_predict1))


confusion_matrix(prueba_y,mnb_predict2)




path='C:/Users/Z820-1/Desktop/Maestría/Matemáticas/Final/Bases/Prueba_V001.csv'
df = pd.read_csv(path)
path1='C:/Users/Z820-1/Desktop/Maestría/Matemáticas/Final/Bases/Prediccion_V001.csv'
prede = pd.read_csv(path1)
#Se define un porcentaje 
porcentaje = 0.80 # Porcentaje de train.
#Selección aleatoria de la muestra de entrenamiento y de prueba
df['entrenamiento'] = np.random.uniform(0, 1, len(df)) <= porcentaje
entrenamiento=df[df['entrenamiento']==True]
entrenamiento.reset_index(drop=True, inplace=True)
prueba=df[df['entrenamiento']==False]
prueba.reset_index(drop=True, inplace=True)
#Se borra la columna creada
del entrenamiento['entrenamiento']
del prueba['entrenamiento']

def limpieza(df1):
        #Se convierten en minúsculas
    df1['name'] = [oracion.lower() for oracion in df1['name']]
    df1['name']=df1['name'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    #Se quitan direcciones
    df1['name']=df1['name'].str.replace(r'https*\S+', "", regex=True)
    #Se quitan usuarios
    df1['name']=df1['name'].str.replace(r'@\S+', "", regex=True)
    #Se la palabra gracias
    df1['name']=df1['name'].str.replace(r'gracais', "", regex=True)
    #Se quitan hashtags
    df1['name']=df1['name'].str.replace(r'#\S+', "", regex=True)
    df1['name']=df1['name'].str.replace(r'\'\w+', "", regex=True)
    #Se quitan signos de puntuación
    df1['name']=df1['name'].str.replace(r'[^\w\s]', "", regex=True)
    #Se quitan dobles espacios
    df1['name']=df1['name'].str.replace(r'\s{2,}', "", regex=True)
    #Se remueven signos especiales
    df1['name']=df1['name'].str.replace(r"\d+", " ", regex=True)
    #============================================
    #Se hace una lista con cada palabra del texto
    #============================================ 
    lista = df1["name"].values.tolist()
    lista1 = df1["Clasificación"].values.tolist()
    #============================================================
    #Las letras que se repiten consecutivamente se quitan por una 
    #============================================================
    repeat_pattern = re.compile(r'(\w)\1*')
    match_substitution = r'\1'
    lista=[repeat_pattern.sub(r'\1', i) for i in lista]
    return lista, lista1

#Entropía cruzada
#Donde p(x) es la probabilidad deseada, y q(x) la probabilidad real. 
#La suma está sobre las tres clases A y B . En este caso 0 y 1. 

def cross_entropy( targets,predictions, epsilon=1e-12):
    """
    Computes cross entropy between targets (encoded as one-hot vectors)
    and predictions. 
    Input: predictions (N, k) ndarray
           targets (N, k) ndarray        
    Returns: scalar
    """
    predictions = np.clip(predictions, epsilon, 1. - epsilon)
    N = predictions.shape[0]
    ce = -np.sum(targets*np.log(predictions+1e-9))/N
    return ce

#Se tienen 2 tablas,una con el texto y otra con la clasificación para la parte de la tabla para entrenar
entrena_x,entrena_y=limpieza(entrenamiento)

#Se tienen 2 tablas,una con el texto y otra con la clasificación para la parte de la tabla para probar
prueba_x,prueba_y=limpieza(prueba)

#Se tienen 2 tablas,una con el texto y otra con la clasificación para la parte de la tabla para probar
prede_x,prede_y=limpieza(prede)


#########################
#Entrenamiento del modelo
#from sklearn.svm import SVC
#param_grid = {'C': [0.1, 1, 10, 100, 1000], 
 #             'gamma': [1, 0.1, 0.01, 0.001, 0.0001],
  #            'kernel': ['rbf']} 

#estimator = GridSearchCV(SVC(), verbose=1, param_grid=param_grid,
#                        scoring='roc_auc', n_jobs=18)

estimator=LogisticRegression()

######################
#Aplicación del modelo
vectorizer = CountVectorizer(stop_words=stopwords.words('spanish'))  
x_vec = vectorizer.fit_transform(entrena_x)
features   = vectorizer.get_feature_names()

tfidfvec = TfidfVectorizer(stop_words=stopwords.words('spanish'))  
x1_vec = tfidfvec.fit_transform(entrena_x)
features1   = vectorizer.get_feature_names()

estimator.fit(x_vec, entrena_y)

x1_gold_vec = tfidfvec.transform(prueba_x)
mnb_predict1 = estimator.predict(x1_gold_vec)
np.unique(mnb_predict1, return_counts=True)

x2_gold_vec = vectorizer.transform(prueba_x)
mnb_predict2 = estimator.predict(x2_gold_vec)
np.unique(mnb_predict2, return_counts=True)


from sklearn.metrics import accuracy_score, f1_score
###################
#Medidas del modelo
#vectorizer
print("Acc MB =", accuracy_score(prueba_y,mnb_predict1))
print("F1 MB =", f1_score(prueba_y, mnb_predict1, average='macro'))
print("micro F1 MB =", f1_score(prueba_y, mnb_predict1, average='micro'))
print("Entropía cruzada", cross_entropy(prueba_y, mnb_predict1))

###################
#Medidas del modelo
#tfidfvec
print("Acc MB =", accuracy_score(prueba_y,mnb_predict2))
print("F1 MB =", f1_score(prueba_y, mnb_predict2, average='macro'))
print("micro F1 MB =", f1_score(prueba_y, mnb_predict2, average='micro'))
print("Entropía cruzada", cross_entropy(prueba_y,mnb_predict1))


confusion_matrix(prueba_y,mnb_predict2)


















